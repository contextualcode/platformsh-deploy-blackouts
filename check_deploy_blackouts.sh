if [ $PLATFORM_BRANCH != "master" ]; then
    exit 0
fi

# platform project:variable:set env:DEPLOY_BLACKOUTS '07:00-12:00;15:20-17:00;22:30-04:00'
TimeSpansString=$DEPLOY_BLACKOUTS
CurrentTime=`expr $(date +%H%M) + 0`
TimeSpans=$(echo $TimeSpansString | tr ";" "\n")

for TimeSpanString in $TimeSpans
do
    # Check if we should ignore deploy blackouts
    # It can be enable by running `platform project:variable:set env:DEPLOY_BLACKOUTS_IGNORE 1`
    if [ $DEPLOY_BLACKOUTS_IGNORE -eq 1 ]; then
        continue
    fi

    # Parse timespan
    IFS='-' read -r -a TimeSpan <<< "$TimeSpanString"
    if [ ${#TimeSpan[@]} -ne 2 ]; then
        echo "[WARNING] Wrong timespan \"$TimeSpanString\", ignoring it."
        continue
    fi
    NextDay=0
    StartTime=${TimeSpan[0]//:/}
    EndTime=${TimeSpan[1]//:/}
    StartTime=`expr $StartTime + 0`
    EndTime=`expr $EndTime + 0`
    if [ $StartTime -gt $EndTime ]; then
        NextDay=1
    fi

    # Check current time against timespan
    Error=0
    if [ $NextDay -eq 1 ]; then
        if [ $CurrentTime -gt $StartTime ] || [ $CurrentTime -lt $EndTime ]; then
            Error=1
        fi
    else
        if [ $CurrentTime -gt $StartTime ] && [ $CurrentTime -lt $EndTime ]; then
            Error=1
        fi
    fi
    if [ $Error -eq 1 ]; then
        echo "[Error] Current time $(date +%H:%M) is within $TimeSpanString deploy blackout timespan" >&2
        exit 1
    fi
done

# Disable DEPLOY_BLACKOUTS_IGNORE, so this check will be not ignored during next build
/app/.platformsh/bin/platform project:variable:set env:DEPLOY_BLACKOUTS_IGNORE 0
